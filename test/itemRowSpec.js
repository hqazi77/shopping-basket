import React from 'react';
import chai from 'chai';
import { shallow } from 'enzyme';
import ItemRow from '../app/components/itemRow';
import sinon from 'sinon';

const expect = chai.expect;

describe('Component: ItemRow', () => {
    let wrapper;
    let itemRow;

    const defaultProps = {
        itemName : 'Apples',
        quantity : 2,
        itemTotalPrice : 50
    };

    beforeEach(() => {
        wrapper = shallow(<ItemRow />);
        itemRow = wrapper.instance();
        wrapper.setProps(defaultProps);
    });

    it('should render', () => {
        expect(wrapper.length).to.equal(1);
    });

    it('should increment', () => {
        const onButtonClick = sinon.spy();

        wrapper.setProps({ onItemQuantityChanged : onButtonClick });

        wrapper.find('#btn-inc').simulate('click');

        expect(onButtonClick.calledOnce).to.equal(true);
        expect(onButtonClick.calledWith(3)).to.equal(true);
    });

    it('should decrement', () => {
        const onButtonClick = sinon.spy();

        wrapper.setProps({ onItemQuantityChanged : onButtonClick });

        wrapper.find('#btn-dec').simulate('click');

        expect(onButtonClick.calledOnce).to.equal(true);
        expect(onButtonClick.calledWith(1)).to.equal(true);
    });

    it('should remove item', () => {
        const onButtonClick = sinon.spy();

        wrapper.setProps({ onItemRemoved : onButtonClick });

        wrapper.find('#btn-remove').simulate('click');

        expect(onButtonClick.calledOnce).to.equal(true);
    });

    it('should change quantity on input', () => {
        const onButtonClick = sinon.spy();

        wrapper.setProps({ onItemQuantityChanged : onButtonClick });

        wrapper.find('#input-qty-change').simulate('change', { target: { value: 10 }});

        expect(onButtonClick.calledOnce).to.equal(true);
        expect(onButtonClick.calledWith(10)).to.equal(true);
    });
});
