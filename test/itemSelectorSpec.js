import React from 'react';
import chai from 'chai';
import { shallow } from 'enzyme';
import ItemSelector from '../app/components/itemSelector';
import sinon from 'sinon';
import _ from 'lodash';

const expect = chai.expect;

describe('Component: ItemSelector', () => {
    let wrapper;
    let itemSelector;

    const itemsData = {
        items : [
        {
            name : "Apples",
            price : 25
        },
        {
            name : "Papayas",
            price : 50,
            promo : {
                promoText : "Three for the price of two",
                amount : 3,
                discount : 50
            }
        }
        ]
    };

    beforeEach(() => {
        wrapper = shallow(<ItemSelector />);
        itemSelector = wrapper.instance();
    });

    it('should render', () => {
        expect(wrapper.length).to.equal(1);
    });

    it('should change state', () => {
        itemSelector.onChange('Apples');
        expect(wrapper.state()).to.deep.equal({ value : 'Apples' });
    });

    it('should add item', () => {
        const onItemAdded = sinon.spy();
        wrapper.setProps({ onItemAdded });
        wrapper.setState({ value : 'Apples' });

        itemSelector.addItem();
        expect(wrapper.state()).to.deep.equal({ value : 'Apples' });
        expect(onItemAdded.calledOnce).to.equal(true);
        expect(onItemAdded.calledWith('Apples')).to.equal(true);
    });

    it('should add item on button click', () => {
        const onItemAdded = sinon.spy();
        wrapper.setProps({ onItemAdded });
        wrapper.setState({ value : 'Apples' });

        wrapper.find('.button-add').simulate('click');

        expect(wrapper.state()).to.deep.equal({ value : 'Apples' });
        expect(onItemAdded.calledOnce).to.equal(true);
        expect(onItemAdded.calledWith('Apples')).to.equal(true);
    });

    it('should get enriched items', () => {
        const enrichedItems = itemSelector.getEnrichedItems(itemsData.items);

        expect(enrichedItems[0]).to.deep.equal(_.merge({
            displayName : 'Apples - 25 ¢'
        }, itemsData.items[0]));

        expect(enrichedItems[1]).to.deep.equal(_.merge({
            displayName : 'Papayas - 50 ¢ (Three for the price of two)'
        }, itemsData.items[1]));
    });

    it('should render correct data', () => {
        wrapper.setState({ value : 'Apples' });
        wrapper.setProps({ items : itemsData.items });

        expect(wrapper.state()).to.deep.equal({ value : 'Apples' });

        const itemSelectorRendered = wrapper.find('.item-selector').children();
        const selector = itemSelectorRendered.at(0);

        expect(itemSelectorRendered).to.have.length(2);
        expect(selector.props().value).to.equal('Apples');
        expect(selector.props().valueKey).to.equal('name');
        expect(selector.props().labelKey).to.equal('displayName');
        expect(selector.props().options)
            .to.deep.equal(itemSelector.getEnrichedItems(itemsData.items));
    });
});
