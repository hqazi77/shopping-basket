import chai from 'chai';
import supertest from 'supertest';

const expect = chai.expect;
const items = require('../data/items.json');

describe('Server', () => {

    const request = supertest('http://localhost:3000');

    before(() => {
        require('../server.js');
    });

    describe('GET /items', () => {
        it('should give item list', done => {
            request
                .get('/items')
                .expect(200, (err, res) => {
                    expect(err).to.be.null;
                    expect(res.body).to.deep.equal(items);
                    done();
                });
        });
    });
});
