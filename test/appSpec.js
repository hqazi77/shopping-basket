import fetch from 'isomorphic-fetch';
import fetchMock from 'fetch-mock';

import React from 'react';
import chai from 'chai';
import { shallow } from 'enzyme';
import _ from 'lodash';
import App from '../app/components/app';

const expect = chai.expect;

describe('Component: App', () => {

    const itemsData = {
        items : [
        {
            name : "Apples",
            price : 25
        },
        {
            name : "Papayas",
            price : 50,
            promo : {
                promoText : "Three for the price of two",
                amount : 3,
                discount : 50
            }
        }
        ]
    };

    let wrapper;
    let app;

    beforeEach((done) => {
        fetchMock.get('http://localhost:3000/items', itemsData);

        wrapper = shallow(<App />);
        app = wrapper.instance();

        app.getItems().then(() => done());
    });

    afterEach(() => {
        fetchMock.restore();
    });

    it('should render', () => {
        expect(wrapper.length).to.equal(1);
    });

    it('should fetch items from api', (done) => {
        app.getItems().then(items => {
            expect(items).to.deep.equal(itemsData.items);
            done();
        });
    });

    it('should add item to state', () => {
        app.addItem(itemsData.items[0]);
        expect(wrapper.state().basketItems).to.deep.equal({
            Apples : { quantity : 1 }
        });
    });

    it('should remove item from state', () => {
        const basketItems = {
            Apples : { quantity : 1 }
        };

        wrapper.setState({ basketItems });

        expect(wrapper.state().basketItems).to.deep.equal(basketItems);
        app.removeItem('Apples');
        expect(wrapper.state().basketItems).to.be.empty;
    });

    it('should change item qty from state', () => {
        const basketItems = {
            Apples : { quantity : 1 }
        };

        wrapper.setState({ basketItems });

        expect(wrapper.state().basketItems).to.deep.equal(basketItems);
        app.changeItemQuantity('Apples', 2);

        expect(wrapper.state().basketItems).to.deep.equal({
            Apples : { quantity : 2 }
        });
    });

    it('should remove item from state if qty is 0', () => {
        const basketItems = {
            Apples : { quantity : 1 }
        };

        wrapper.setState({ basketItems });

        expect(wrapper.state().basketItems).to.deep.equal(basketItems);
        app.changeItemQuantity('Apples', 0);

        expect(wrapper.state().basketItems).to.be.empty;
    });

    it('should calculate basket total price', () => {
        const basketItems = {
            Apples : { quantity : 1 },
            Papayas : { quantity : 3 }
        };

        wrapper.setState({ basketItems });

        expect(wrapper.state().basketItems).to.deep.equal(basketItems);
        const basketTotalPrice = app.calculateBasketTotalPrice();
        expect(basketTotalPrice).to.equal(125);
    });

    it('should calculate item total price', () => {
        const itemTotalPrice = app.calculateItemTotalPrice(itemsData.items[0], 3);
        expect(itemTotalPrice).to.equal(75);
    });

    it('should generate correct item rows', () => {
        const basketItems = {
            Apples : { quantity : 1 },
            Papayas : { quantity : 3 }
        };

        wrapper.setState({ basketItems });

        expect(wrapper.state().basketItems).to.deep.equal(basketItems);

        const itemRows = wrapper.find('tbody').children();

        expect(itemRows).to.have.length(2);
        expect(itemRows.at(0).props().quantity).to.equal(basketItems.Apples.quantity);
        expect(itemRows.at(0).props().itemName).to.equal('Apples');
        expect(itemRows.at(0).props().itemTotalPrice).to.equal(25);
        expect(itemRows.at(1).props().quantity).to.equal(basketItems.Papayas.quantity);
        expect(itemRows.at(1).props().itemName).to.equal('Papayas');
        expect(itemRows.at(1).props().itemTotalPrice).to.equal(100);
    });
});
