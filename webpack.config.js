var webpack = require('webpack');

module.exports = {
    entry: './app/index.js',
    output: {
        path: './public',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'ENV_PORT': JSON.stringify(process.env.PORT)
        })
    ],
    resolve: {
        extensions: ['', '.js', '.json']
    }
};
