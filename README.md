# Shopping basket

This is a simple React shopping basket application.

This was run using node version 4.2.4 and npm version 3.10.8.

## Instructions

Within the repo run `npm i` to install dependencies.

`npm run dev` starts the dev server with webpack watch.

`npm run build` will build the React app to the public folder. After that you can run `npm start` to start the server on port 3000.

`npm test` will run the test suite.


## Considerations

I was recommended to keep this simple. Also time constraints prevented me from further improvements. If I was writing this for a production release, here are a few required improvements I would make:

* **Adding ENV based config** - At the moment there are some hard coded values that belong in config that could vary based on environment.

* **Running the server on a cluster** - In order to take advantage of multi core processors.

* **Caching** - Both server and client side for the items

* **Database** - The items and related data should be in a db exposed via the GET items api.

* **Stylising** - The spec mentioned UI was not important so no focus was made on style.

* **Testing** - More tests could be added, specifically in terms of correct content rendered and edge/ failure case (e.g. the GET items request fails).

* **Linting** - Adding an eslint config file to make sure coding style meets standard.
