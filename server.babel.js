import express from 'express';

const items = require('./data/items.json');
const app = express();

app.use('/', express.static('public'));

app.get('/items', (req, res) => {
    res.json(items);
});

app.listen(3000);
