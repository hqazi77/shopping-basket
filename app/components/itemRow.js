import React from 'react';

class ItemRow extends React.Component {
    constructor (props) {
        super(props);

        this.state = {};
    }

    handleChange (event) {
        const quantity = parseInt(event.target.value);

        if (!isNaN(quantity)){
            this.props.onItemQuantityChanged(quantity);
        }
    }

    increment () {
        this.props.onItemQuantityChanged(this.props.quantity + 1);
    }

    decrement () {
        this.props.onItemQuantityChanged(this.props.quantity - 1);
    }

    render () {
        return (
            <tr className="item-row">
                <td key = { this.props.itemName }>
                    { this.props.itemName }
                </td>
                <td>
                    <button id="btn-dec" onClick = { this.decrement.bind(this) }>
                        -
                    </button>
                </td>
                <td className="item-row-input">
                    <input
                      id="input-qty-change"
                      type = 'text'
                      value = { this.props.quantity }
                      onChange = { this.handleChange.bind(this) } />
                </td>
                <td>
                    <button id="btn-inc" onClick = { this.increment.bind(this) }>
                          +
                    </button>
                </td>
                <td>
                    <button
                      id="btn-remove"
                      onClick  = { this.props.onItemRemoved }>
                      Remove
                    </button>
                </td>
                <td className="item-row-price">
                    $ { (this.props.itemTotalPrice / 100).toFixed(2) }
                </td>
            </tr>
            );
    }
}

ItemRow.propTypes = {
    quantity : React.PropTypes.number,
    itemName : React.PropTypes.string,
    itemTotalPrice : React.PropTypes.number,
    onItemRemoved : React.PropTypes.func,
    onItemQuantityChanged : React.PropTypes.func
};

export default ItemRow;
