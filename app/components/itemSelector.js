import React from 'react';
import Select from 'react-select';
import _ from 'lodash';

class ItemSelector extends React.Component {
    constructor (props) {
        super(props);

        this.state = {};

        this.onChange = this.onChange.bind(this);
    }

    onChange (value) {
        this.setState({ value });
    }

    addItem () {
        this.props.onItemAdded(this.state.value);
    }

    getEnrichedItems (items) {
        return _.map(items, item => {
            let displayName = item.name + ' - ' + item.price + ' ¢';
            if (item.promo) displayName += ' (' + item.promo.promoText + ')';
            return _.merge({ displayName }, item);
        });
    }

	render () {
		return <div className="item-selector">
            <Select
                name="form-field-name"
                value={ this.state.value }
                valueKey="name"
                labelKey="displayName"
                options={ this.getEnrichedItems(this.props.items) }
                onChange={ this.onChange }
            />
            <button className="button-add" onClick = { this.addItem.bind(this) }>
                    Add
            </button>
		</div>;
	}
}

ItemSelector.propTypes = {
    items : React.PropTypes.array,
    onItemAdded : React.PropTypes.func
};

export default ItemSelector;
