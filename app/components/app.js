import React from 'react';
import ItemSelector from './itemSelector.js';
import ItemRow from './itemRow.js';
import _ from 'lodash';

export default class App extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            basketItems : {},
            items : []
        };

        this.removeItem = this.removeItem.bind(this);
        this.addItem = this.addItem.bind(this);
        this.getItemRows = this.getItemRows.bind(this);
        this.calculateBasketTotalPrice = this.calculateBasketTotalPrice.bind(this);

        this.getItems().then(items => this.setState({ items }));
    }

    getItems () {
        return fetch('http://localhost:3000/items')
            .then(response => response.json())
            .then(json => json.items);
    }

    addItem (item) {
        const itemName = _.get(item, 'name');
        const basketItems = _.cloneDeep(this.state.basketItems);

        if (_.find(this.state.items, { name : itemName })){
            if (basketItems[itemName]){
                basketItems[itemName].quantity++;

            } else {
                basketItems[itemName] = { quantity : 1 };
            }

            this.setState({ basketItems });
        }
    }

    removeItem (itemName) {
        if (this.state.basketItems[itemName]){
            this.setState({
                basketItems : _.omit(this.state.basketItems, itemName)
            });
        }
    }

    changeItemQuantity (itemName, quantity) {
        if (quantity < 0) return;

        if (quantity === 0) return this.removeItem(itemName);

        if (this.state.basketItems[itemName]){
            const basketItems = _.cloneDeep(this.state.basketItems);
            basketItems[itemName] = { quantity };
            this.setState({ basketItems });
        }
    }

    calculateBasketTotalPrice () {
        const items = this.state.items;
        return _.chain(this.state.basketItems)
            .keys()
            .reduce((sum, k) => {
                const quantity = this.state.basketItems[k].quantity;
                const price = this.calculateItemTotalPrice(_.find(items, { name : k }), quantity);
                return sum + price;
            }, 0)
            .value();
    }

    calculateItemTotalPrice (item, quantity) {
        if (!item) return;

        const totalPrice = item.price * quantity;
        if (!item.promo) return totalPrice;

        const totalDiscount = item.promo.discount * Math.floor(quantity / item.promo.amount);
        return totalPrice - totalDiscount;
    }

    getItemRows () {
        const items = this.state.items;
        return _.chain(this.state.basketItems)
            .keys()
            .map(k => {
                const quantity = this.state.basketItems[k].quantity;
                return <ItemRow
                        quantity = { quantity }
                        key = { k }
                        itemName = { k }
                        itemTotalPrice = {
                            this.calculateItemTotalPrice(_.find(items, { name : k }), quantity)
                        }
                        onItemQuantityChanged = { this.changeItemQuantity.bind(this, k) }
                        onItemRemoved = { this.removeItem.bind(this, k) }
                    />
            })
            .value();
    }

    render () {
        const basketItems = this.state.basketItems;
        const itemSelectSection = this.state.items.length ?
            <ItemSelector items = { this.state.items } onItemAdded = { this.addItem } /> :
            <div>Loading...</div>;

        const itemRows = this.getItemRows();

        return (
            <div>
                <h1>Shopping Basket</h1>
                { itemSelectSection }
                <table className="items-list">
                 <tbody>
                { itemRows }
                 </tbody>
                </table>
                <div className="total-section">
                <h2>Total</h2>
                <p> $ { (this.calculateBasketTotalPrice() / 100).toFixed(2) } </p>
                </div>
            </div>
        );
    }
}
